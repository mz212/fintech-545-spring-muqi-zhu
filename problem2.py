import pandas as pd
import numpy as np
import statsmodels.api as sm
from scipy.optimize import minimize
from scipy.stats import t
import matplotlib.pyplot as plt
from scipy.stats import multivariate_normal
from sklearn.linear_model import LinearRegression

# a
# get the data in problem2.csv
data1 = pd.read_csv('problem2.csv')
x = data1['x']
y = data1['y']
n = len(y)
    
# add a constant term column to the design matrix X
X = np.column_stack((np.ones(n), x))

# fit data using OLS
X1 = sm.add_constant(X)
fit_OLS = sm.OLS(y, X).fit()

# beta  and standard deviation of OLS
beta_OLS = fit_OLS.params
std_OLS = fit_OLS.bse['const']
print(f'Beta (OLS): {beta_OLS},\n Standard Deviation of OLS Error: {std_OLS}')


# log-likelihood function under normality distribution assumption
def log_likelihood_normal(params, X, Y):
    beta = params[:-1]
    sigma_squared = params[-1]
    residuals = Y - np.dot(X, beta)
    ll = -0.5 * n * np.log(2 * np.pi * sigma_squared) - 0.5 * np.sum(residuals**2) / sigma_squared
    return -ll  

# Initialize parameter 
pa_normal = np.zeros(X.shape[1] + 1)  

# Maximum likelihood estimation using the minimization function under normality distribution assumption
result_normal = minimize(log_likelihood_normal, pa_normal, args=(X, y), method='Nelder-Mead')

# get beta and standard deviation we need
beta_normal = result_normal.x[:-1]
std_normal = result_normal.x[-1]

# print the result
print("Normal Distribution MLE:")
print("Beta (MLE):", beta_normal)
print("Standard Deviation of MLE Error:", np.sqrt(std_normal))

#b
# log-likelihood function under T distribution assumption
def log_likelihood_t(params, X, Y):
    beta = params[:-2]
    sigma_squared = params[-2]
    nu = params[-1]
    residuals = Y - np.dot(X, beta)
    ll = np.sum(t.logpdf(residuals / np.sqrt(sigma_squared), df=nu)) - 0.5 * n * np.log(2 * np.pi * sigma_squared)
    return -ll  

# Initialize parameter 
pa_t = np.ones(X.shape[1] + 2)

# Maximum likelihood estimation using the minimization function under T distribution assumption
result_t = minimize(log_likelihood_t, pa_t, args=(X, y), method='L-BFGS-B')

# Maximum likelihood estimation using the minimization function under T distribution assumption
beta_t = result_t.x[:-2]
std_t = result_t.x[-2]
nu = result_t.x[-1]

print("\nT Distribution MLE:")
print("Beta (MLE):", beta_t)
print("Standard Deviation of MLE Error:", np.sqrt(std_t))
print("Degrees of Freedom (nu):", nu)

# c
# get the data
data = pd.read_csv('problem2_x.csv')
data_x = pd.read_csv('problem2_x1.csv')

# get the data used to train
X_train = data[['x1']]  # Use double square brackets to create a DataFrame instead of a Series
y_train = data['x2']

# use the regression model to fit x1 and x2
model = LinearRegression()
model.fit(X_train, y_train)

# prepare x2 of given x1 from problem2_x1.csv
X_test = data_x[['x1']]
y_pred = model.predict(X_test)

# calculate the expectation and 95% confidence interval of predictions
stderr = np.std(y_pred, ddof=1) / np.sqrt(len(data))
confidence_interval = t.interval(0.95, df=len(data)-1, loc=y_pred, scale=stderr)
# print the confidence interval bounds
print("Lower Bound of Confidence Interval:", confidence_interval[0][0])
print("Upper Bound of Confidence Interval:", confidence_interval[1][0])

# plot the expectation and confidence interval
plt.scatter(data_x['x1'], y_pred, label='Expected Value', color='red')
plt.fill_between(data_x['x1'], confidence_interval[0], confidence_interval[1], color='blue', alpha=0.3, label='95% Confidence Interval')

plt.xlabel('X1')
plt.ylabel('X2')
plt.legend()
plt.show()