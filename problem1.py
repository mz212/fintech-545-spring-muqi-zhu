import pandas as pd
import numpy as np
from scipy.stats import moment, skew, kurtosis

# problem 1
# get the data
data = pd.read_csv('problem1.csv')
data = data['x']

# the length of data we use
n = len(data)

# a. use the normalized formula to calculate the first four moments
mean1 = sum(data) / n
variance1 = sum((x - mean1) ** 2 for x in data) / (n-1)
std = variance1 ** 0.5
skewness1 = n * sum((x - mean1) ** 3 / std** 3 for x in data) / ((n-1) * (n-2))
# square of the biased estimator for variance.
sig4 = (sum((x - mean1) ** 2 for x in data) / (len(data))) ** 2
# biased estimator for kurtosis
k4 = np.mean(sum((x - mean1) ** 4/len(data) for x in data))
kurtosis1 = n**2 * ((n*(n-1)**2 + (6*n-9)) * k4 - n * (6*n-9) * sig4) / ((n-1)**3 * (n**2-3*n+3) * std**4)

print("a. Moments using normalized formula:")
print(f" mean: {mean1}")
print(f" variance: {variance1}")
print(f" skewness: {skewness1}")
print(f" kurtosis: {kurtosis1}")

# b. use statistical package to calculate the first four moments
mean2 = np.mean(data)
variance2 = np.var(data, ddof=1)
skewness2 = skew(data)
kurtosis2 = kurtosis(data)

print("b. Moments using chosen statistical package:")
print(f" mean: {mean2}")
print(f" variance: {variance2}")
print(f" skewness: {skewness2}")
print(f" kurtosis: {kurtosis2}")

