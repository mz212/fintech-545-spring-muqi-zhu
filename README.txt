--------------------------Problem1--------------------------
---Description---
This code calculates the first four moments (mean, variance, skewness, and kurtosis) of a given dataset using both a normalized formula and a statistical package.

---Getting Started---
Follow these instructions to run the code on your local machine.

---Prerequisites---
1.Python 3.x
2.Required Python libraries: pandas, numpy, scipy

---Running the Code---
1.Clone the repository or download the script problem1.py.
2.Open a terminal and navigate to the script's directory.
3.Run the script using the following command:

python problem1.py

The script will output the calculated moments using both the normalized formula and a statistical package.

---Usage---
1.The script reads the dataset from a CSV file named problem1.csv.
2.It calculates the moments using a normalized formula (a.) and a statistical package (b.).
3.The results are printed to the console.


--------------------------Problem2--------------------------
---Description---
This code performs regression analysis and Maximum Likelihood Estimation (MLE) on given datasets. The code is divided into three sections:

1.Use OLS and MLE (under normality assumption) to fit data in problem2.
2.Compare the MLE fitting result under normality distribution and T distribution.
3.Fit data in problem2_x.csv and use it to predict x2 for data in problem2_x1.csv.

---Getting Started---
Follow these instructions to run the code on your local machine.

---Prerequisites---
1.Python 3.x
2.Required Python libraries: pandas, numpy, statsmodels, scipy, matplotlib, scikit-learn

---Running the Code---
1.Clone the repository or download the script problem2.py.
2.Open a terminal and navigate to the script's directory.
3.Run the script using the following command:

python problem2.py

The script will output the calculated moments using both the normalized formula and a statistical package.

---Usage---
1.The script reads datasets from CSV files (problem2.csv, problem2_x.csv, problem2_x1.csv).
2.It performs OLS regression, MLE under normal distribution, and MLE under Student's t distribution.
3.Results are printed to the console, including beta coefficients, standard deviations, and degrees of freedom.
4.A plot is generated for the expectation and a 95% confidence interval.

--------------------------Problem3--------------------------
---Description---
This code fits AutoRegressive Integrated Moving Average (ARIMA) models to a given time series dataset (problem3.csv). It explores various AR(p) and MA(q) models and prints Akaike Information Criterion (AIC) values along with parameter estimates.

---Getting Started---
Follow these instructions to run the code on your local machine.

---Prerequisites---
1.Python 3.x
2.Required Python libraries: pandas, statsmodels

---Running the Code---
1.Clone the repository or download the script problem3.py.
2.Open a terminal and navigate to the script's directory.
3.Run the script using the following command:

python problem3.py

The script will output the calculated moments using both the normalized formula and a statistical package.

---Usage---
1.The script reads a time series dataset from a CSV file (problem3.csv).
2.It fits AR(p) models and MA(q) models for specified values of p and q.
3.AIC values and parameter estimates are printed for each model.