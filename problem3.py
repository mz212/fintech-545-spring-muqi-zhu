import pandas as pd
import statsmodels.api as sm

# get the data
data = pd.read_csv('problem3.csv')
x = data['x']

# Fit the AR(1) to AR(3) models
for p in range(1, 4):
    model = sm.tsa.ARIMA(x, order=(p, 0, 0))
    results = model.fit()
    print(f'AR({p}) AIC of model: {results.aic}')
    print(f'AR({p}) parameter of model: {results.params}')

# Fit the MA(1) to MA(3) models
for q in range(1, 4):
    model = sm.tsa.ARIMA(x, order=(0, 0, q))
    results = model.fit()
    print(f'MA({q}) AIC of model: {results.aic}')
    print(f'MA({q}) parameter of model: {results.params}')
    
# View the AR(3) model parameter estimates and standard errors
model = sm.tsa.ARIMA(x, order=(3, 0, 0))
results = model.fit()
print(results.summary())
